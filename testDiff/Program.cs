﻿using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace testDiff
{
    class Program
    {
        static void Main(string[] args)
        {
            string OldText = @"美國時間10月21日，Google母公司Alphabet旗下的Project Loon項目有了新進展。

該項目的負責人Alastair Westgarth發文表示，Project Loon的氣球正搭載通信設備，在波多黎各上空為當地提供通信服務，並首次使用人工智能來控制其漂浮狀態。
Project Loon的氣球由地面一個大型的發射塔來放出升空，通過與地面通信系統進行配合，在空中發揮手機信號發射塔的作用，為當地用戶提供通訊服務。

這些氣球的主體由聚乙烯帆布製成，每個有網球場那麼大，在距離地面約20公里的同溫層中工作氣球下方掛載通訊設備，並使用太陽能作為動力，能持續在空中停留100天，最長記錄曾長達190天。

據負責人介紹，這次漂浮在波多黎各的氣球，首次使用機器學習算法來控制它們的位置。他們能夠將多個氣球組成編隊，形成小型網絡。

讓氣球長時間地滯留在預定的空域，最大的挑戰就是風。由於風在大氣中是分層的，每一層的風，都有其速度和方向。項目Loon通過對風進行分析學習，進而做出決策，讓氣球的編隊能夠保持足夠的數量，並停留在目的地上空，以完成信號覆蓋。

而退役的氣球會被回收，降落在預定地點後，由工作人員前往收集。

9月，美屬波多黎各遭受颶風重創，大量移動網絡基礎設備被破壞，通信出現大面積癱瘓。據美國聯邦通信委員會（FCC）聲明，在10月初，波多黎各約有82％的地區沒有手機信號。給當地居民提供急需的通訊服務，FCC宣布用Project Loon這樣的“創新方式幫助恢復這座島嶼上的通信”。

聯邦應急管理局（FAA），聯邦應急管理局（FEMA），美國聯邦航空管理局（FAA），美國聯邦航空管理局（FAA），聯邦應急管理局以及通信運營商AT＆T等多方面的支持。


今年3月，秘魯受水災影響，通信受到破壞，該項目與當地政府及運營商進行合作，在72小時內為4萬平方公里提供了超過160GB數據流量的臨時通信服務。數万用戶提供通信服務。

Project Loon成立於2008年，是早前Google X實驗室的其中一個項目，致力於通過高空氣球為邊遠地區提供互聯網通信服務.2013年，項目Loon在新西蘭進行首次大規模試驗。";

            string NewText = @"美國時間 10 月 21 日，Google 母公司 Alphabet 旗下的 Project Loon 計畫有了新進展。

該計畫的負責人 Alastair Westgarth 發文表示，Project Loon 的氣球正搭載通訊設備，在波多黎各上空為當地提供通訊服務，並首次使用人工智慧來控制其漂浮狀態。

Project Loon 的氣球由地面一個大型的發射塔來放出升空，通過與地面通訊系統進行配合，在空中發揮手機信號發射塔的作用，為當地用戶提供通訊服務。

這些氣球的主體由聚乙烯帆布製成，每個有網球場那麼大，在距離地面約 20 公里的同溫層中工作。氣球下方掛載通訊設備，並使用太陽能作為動力，能持續在空中停留 100 天，最長記錄曾長達 190 天。

據負責人介紹，這次漂浮在波多黎各的氣球，首次使用機器學習算法來控制它們的位置。他們能夠將多個氣球組成編隊，形成小型網路。

讓氣球長時間地滯留在預定的空域，最大的挑戰就是風。由於風在大氣中是分層的，每一層的風，都有其速度和方向。Project Loon 透過對風進行分析學習，進而做出決策，讓氣球的編隊能夠保持足夠的數量，並停留在目的地上空，以完成訊號覆蓋。

而退役的氣球會被回收，降落在預定地點後，由工作人員前往收集。

9 月，美屬波多黎各遭受颶風重創，大量行動網路基礎設備被破壞，通訊出現大面積癱瘓。據美國聯邦通訊委員會（FCC）聲明，在 10 月初，波多黎各約有 82% 的地區沒有手機信號。為了給當地居民提供急需的通訊服務，FCC 宣布用 Project Loon 這樣的「創新方式幫助恢復這座島嶼上的通訊」。

這次 Project Loon 能夠快速地部署並提供服務，除了此前的多次實戰，更重要的是獲得了美國聯邦通訊委員會（FCC）、美國聯邦航空管理局（FAA）、聯邦應急管理局（FEMA），以及通訊商 AT&T 等多方面的支援。

今年 3 月，秘魯受水災影響，通訊受到破壞，該計畫與當地政府及運營商進行合作，在 72 小時內為 4 萬平方公里提供了超過 160GB 數據流量的臨時通訊服務。這是 Project Loon 首次為數萬用戶提供通訊服務。

";

            //UseInlineDiff(OldText, NewText);

            //UseSideBySideDiffByNewTexts(OldText, NewText);

            //UseSideBySideDiffByTwoTexts(OldText, NewText);

            Userdiff_match_patch(OldText, NewText);
            
            Console.ReadLine();
        }

        public static void UseInlineDiff(string OldText, string NewText)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"start {nameof(UseInlineDiff)}");

            List<string> OldTexts = SplitLine(OldText, 15);
            string OldTextWithFilter = MergeLine(OldTexts);
            List<string> NewTexts = SplitLine(NewText, 15);
            string NewTextWithFilter = MergeLine(NewTexts);

            var diffBuilder = new InlineDiffBuilder(new Differ());
            var diff = diffBuilder.BuildDiffModel(OldTextWithFilter, NewTextWithFilter);

            foreach (var line in diff.Lines)
            {
                switch (line.Type)
                {
                    case ChangeType.Inserted:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"<ins>{line.Text}</ins>");
                        break;
                    case ChangeType.Deleted:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"<del>{line.Text}</del>");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine(line.Text);
                        break;
                }

                Console.WriteLine();
            }
        }

        public static void UseSideBySideDiffByNewTexts(string OldText, string NewText)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"start {nameof(UseSideBySideDiffByNewTexts)}");

            OldText = OldText.Replace(" ", "");
            NewText = NewText.Replace(" ", "");
            List<string> OldTextsOfSideBySide = SplitString(OldText);
            string OldTextWithFilterOfSideBySide = MergeSpace(OldTextsOfSideBySide);
            List<string> NewTextsOfSideBySide = SplitString(NewText);
            string NewTextWithFilterOfSideBySide = MergeSpace(NewTextsOfSideBySide);

            // 自訂換行
            //List<string> OldTexts = SplitLine(OldTextWithFilterOfSideBySide, 30);
            //string OldTextWithFilter = MergeLine(OldTexts);
            //List<string> NewTexts = SplitLine(NewTextWithFilterOfSideBySide, 30);
            //string NewTextWithFilter = MergeLine(NewTexts);

            var diffBuilderOfSideBySide = new SideBySideDiffBuilder(new Differ());
            var diffOfSideBySide = diffBuilderOfSideBySide.BuildDiffModel(OldTextWithFilterOfSideBySide, NewTextWithFilterOfSideBySide);

            foreach (var lines in diffOfSideBySide.NewText.Lines)
            //foreach (var lines in diffOfSideBySide.OldText.Lines)
            {
                if (string.IsNullOrEmpty(lines.Text) || string.IsNullOrEmpty(lines.Text.Replace("\t", "")))
                {
                    continue;
                }

                string linesOfFilter = lines.Text.Replace("\t", "");

                switch (lines.Type)
                {
                    case ChangeType.Inserted:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"<ins>{linesOfFilter}</ins>");
                        break;
                    case ChangeType.Deleted:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($"<del>{linesOfFilter}</del>");
                        break;
                    case ChangeType.Unchanged:
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(linesOfFilter);
                        break;
                    case ChangeType.Imaginary:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write($"<img>{linesOfFilter}</img>");
                        break;
                    case ChangeType.Modified:

                        foreach (var line in lines.SubPieces)
                        {
                            if (string.IsNullOrEmpty(line.Text) || string.IsNullOrEmpty(line.Text.Replace("\t", "")))
                            {
                                continue;
                            }

                            string lineOfFilter = line.Text.Replace("\t", "");

                            switch (line.Type)
                            {
                                case ChangeType.Inserted:
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.Write($"<ins>{lineOfFilter}</ins>");
                                    break;
                                case ChangeType.Deleted:
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write($"<del>{lineOfFilter}</del>");
                                    break;
                                case ChangeType.Unchanged:
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.Write(lineOfFilter);
                                    break;
                            }
                        }

                        break;
                }
            }

            Console.WriteLine();
        }

        public static void UseSideBySideDiffByTwoTexts(string OldText, string NewText)
        {
            var result = new StringBuilder();

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine($"start {nameof(UseSideBySideDiffByTwoTexts)}");

            OldText = OldText.Replace(" ", "");
            NewText = NewText.Replace(" ", "");

            var diffBuilderOfSideBySide = new SideBySideDiffBuilder(new MyDiffer());
            var diffOfSideBySide = diffBuilderOfSideBySide.BuildDiffModel(OldText, NewText);

            for (int i = 0; i < diffOfSideBySide.OldText.Lines.Count; i++)
            {
                string linesByOld = diffOfSideBySide.OldText.Lines[i].Text;

                if (!string.IsNullOrEmpty(linesByOld))
                {
                    switch (diffOfSideBySide.OldText.Lines[i].Type)
                    {
                        case ChangeType.Deleted:
                            {
                                result.Append($"<p><del>{linesByOld}</del></p>");
                                break;
                            }
                    }
                }

                string linesByNew = diffOfSideBySide.NewText.Lines[i].Text;

                if (string.IsNullOrEmpty(linesByNew))
                {
                    continue;
                }

                switch (diffOfSideBySide.NewText.Lines[i].Type)
                {
                    case ChangeType.Inserted:
                        {
                            result.Append($"<p><ins>{linesByNew}</ins></p>");
                            break;
                        }
                    case ChangeType.Unchanged:
                        {
                            result.Append($"<p>{linesByNew}</p>");
                            break;
                        }
                    case ChangeType.Modified:
                        {
                            result.Append("<p>");

                            StringBuilder oldBuffer = new StringBuilder();
                            StringBuilder newBuffer = new StringBuilder();

                            for (var j = 0; j < diffOfSideBySide.OldText.Lines[i].SubPieces.Count; j++)
                            {
                                switch (diffOfSideBySide.OldText.Lines[i].SubPieces[j].Type)
                                {
                                    case ChangeType.Deleted:
                                        {
                                            string lineByOld = diffOfSideBySide.OldText.Lines[i].SubPieces[j].Text;

                                            if (!string.IsNullOrEmpty(lineByOld))
                                            {
                                                oldBuffer.Append($"<del>{lineByOld}</del>");
                                            }

                                            break;
                                        }
                                }

                                string lineByNew = diffOfSideBySide.NewText.Lines[i].SubPieces[j].Text;

                                if (string.IsNullOrEmpty(lineByNew))
                                {
                                    continue;
                                }

                                switch (diffOfSideBySide.NewText.Lines[i].SubPieces[j].Type)
                                {
                                    case ChangeType.Inserted:
                                        {
                                            newBuffer.Append($"<ins>{lineByNew}</ins>");
                                            break;
                                        }
                                    case ChangeType.Unchanged:
                                        {
                                            FreedBuffer(ref result, ref oldBuffer, ref newBuffer);

                                            result.Append(lineByNew);

                                            break;
                                        }
                                }
                            }

                            FreedBuffer(ref result, ref oldBuffer, ref newBuffer);

                            result.Append("</p>");

                            break;
                        }
                }
            }

            result = result.Replace("</del><del>", "").Replace("</ins><ins>", "");

            Console.WriteLine(result.ToString());
            Console.WriteLine();
        }

        public static void Userdiff_match_patch(string OldText, string NewText)
        {
            var dmp = new diff_match_patch();
            var result = dmp.diff_prettyHtml(dmp.diff_main(OldText, NewText));
            Console.WriteLine(result.ToString());
            Console.WriteLine();
        }

        public static List<string> SplitLine(string line, int lineNumber)
        {
            return new List<string>(
                Regex.Split(line, $"(?<=\\G.{{{lineNumber}}})", RegexOptions.Singleline)
                .Where(s => !string.IsNullOrEmpty(s)));
        }

        public static List<string> SplitString(string line)
        {
            return new List<string>(
                Regex.Split(line, "", RegexOptions.None)
                .Where(s => !string.IsNullOrEmpty(s)));
        }

        public static string MergeSpace(List<string> lines)
        {
            return string.Join("\t", lines);
        }

        public static string MergeLine(List<string> lines)
        {
            return string.Join("\r\n", lines);
        }

        public static void FreedBuffer(ref StringBuilder result, ref StringBuilder oldBuffer, ref StringBuilder newBuffer)
        {
            if (oldBuffer.Length > 0)
            {
                result.Append(oldBuffer.ToString());
                oldBuffer.Clear();
            }

            if (newBuffer.Length > 0)
            {
                result.Append(newBuffer.ToString());
                newBuffer.Clear();
            }
        }
    }
}
